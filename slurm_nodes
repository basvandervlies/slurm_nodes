#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Author: Martijn Kruiten
# Date  : 11th of June 2018
#
# Code is based on sara_nodes by Dennis Stam (SURFsara, 2012)
#
'''
You can use slurm_nodes to change the state of a node with an optional
reference to a ticket number.
'''

from __future__ import print_function
from collections import OrderedDict
import pyslurm
import re
import sys
import time
import pwd
import argparse

# Optional variable to split nodes based on basename
SPLIT_BASE = r'r\d+n\d+'

# Lengths to truncate columns
TRUNC = {'REASON': 23,
         'TICKET': 7,
         'NODELIST': 58}

# States to show in overview
DOWN_STATES = set(['DOWN',
                   'DRAIN',
                   'MAINT',
                   'UNKNOWN',
                   'ERROR',
                   'FAIL',
                   'FAILING'])

# Global variables set by input arguments
ARGS_QUIET = False
ARGS_DEBUG = False
ARGS_VERBOSE = False
ARGS_IGNORE = False
ARGS_DRYRUN = False
ARGS_EXPAND = False
ARGS_LONG = False


def expand_nodes(nodelist):
    '''Expand node range expression to list of nodes'''
    nodes = pyslurm.hostlist()
    nodestr = ','.join(nodelist)
    nodes.create(nodestr)
    nodes.uniq()
    nodelist = nodes.get_list()

    # Remove non-existing nodes from list
    server = pyslurm.node()
    for node in nodelist:
        if not server.get_node(node):
            nodelist.remove(node)
            print('Cannot find node %s' % node, file=sys.stderr)

    return nodelist


def compact_nodes(nodelist):
    '''Compact list of nodes to single host range expression'''
    nodes = pyslurm.hostlist()
    nodestr = ','.join(nodelist)
    nodes.create(nodestr)

    return (nodes.count(), nodes.ranged_string())


def get_exec_nodes(jobs):
    '''Get nodes belonging to specified job(s)'''
    s = pyslurm.job()
    nodes = list()

    for job in jobs:
        try:
            exec_hosts = s.find_id(job)[0]['nodes']
        except:
            print('Cannot find job %s' % job, file=sys.stderr)
            continue
        nodes.append(exec_hosts)

    nodes = expand_nodes(nodes)
    return nodes


def generate_index(string):
    '''Generate index to sort numerically'''
    return [int(y) if y.isdigit() else y for y in re.split(r'(\d+)', string)]


def get_nodes(hosts=None):
    '''Parse nodes and split based on SPLIT_BASE match'''
    if ARGS_DEBUG:
        print('func:get_nodes input:%s' % str(hosts), file=sys.stderr)

    match = dict()
    rest = dict()

    if ARGS_DEBUG:
        print('func:get_nodes fetching node information',
              file=sys.stderr)

    # Get all nodes with their properties
    s = pyslurm.node()
    for host, properties in s.get().items():
        selected = None
        # Split states and remove non alphanumeric characters
        states = properties['state'].split('+')
        states = [re.sub(r'\W+', '', state) for state in states]
        # Select nodes that were specified by the user
        if hosts and host in hosts:
            selected = host
        elif hosts is None:
            # Select nodes based on node state
            if bool(DOWN_STATES.intersection(set(states))):
                selected = host
            # Select nodes that are online but have no partition
            elif not properties['partitions']:
                properties['reason'] = 'No partition'
                selected = host

        # Split selected hosts based on basename
        if selected:
            if SPLIT_BASE and re.findall(SPLIT_BASE, selected):
                match[host] = properties
            else:
                rest[host] = properties

    if ARGS_DEBUG:
        print('func:get_nodes returning values', file=sys.stderr)
    return match, rest


def parse_properties(dictin):
    '''Parse the properties into a common format that can be printed'''
    if ARGS_DEBUG:
        print('func:parse_properties input:%s'
              % str(dictin), file=sys.stderr)

    printline = list()
    if ARGS_DEBUG:
        print('func:parse_properties processing data', file=sys.stderr)

    groups = OrderedDict()
    timeformat = '%d %b %H:%M'

    for host in sorted(dictin.keys(), key=generate_index):
        # Skip nodes without partition in ignore mode
        if ARGS_IGNORE and not dictin[host]['partitions']:
            continue

        # Create a dictionary with node properties
        props = dict()
        props['nodename'] = [host]
        props['state'] = dictin[host]['state'].lower()
        props['date'] = dictin[host]['reason_time']
        try:
            props['username'] = pwd.getpwuid(dictin[host]['reason_uid'])[0]
        except TypeError, detail:
            props['username'] = ''

        reason = dictin[host]['reason']
        if reason:
            reasonstate = reason + '|' + props['state']
        else:
            reasonstate = props['state']

        # Add to group if reason and state combi already exists
        if reasonstate in groups and not ARGS_EXPAND:
            groups[reasonstate]['nodename'] += props['nodename']
            if props['date'] > groups[reasonstate]['date']:
                groups[reasonstate]['date'] = props['date']
        else:
            # Split reason into note and ticket number
            if reason:
                note = reason.split('+')
            else:
                note = ['']

            if len(note) > 1:
                props['ticket'] = note[0]
                props['note'] = ", ".join(note[1:])
            else:
                # This is a Slurm generated reason without a ticket number
                props['ticket'] = ''
                props['note'] = note[0]

            if ARGS_EXPAND:
                props['count'] = 1
                props['nodename'] = props['nodename'][0]
                props['date'] = time.strftime(timeformat,
                                              time.localtime(props['date']))
                printline.append(props)
            else:
                # Create group for reason and state combination
                groups[reasonstate] = props

    if not ARGS_EXPAND:
        # Add lines for node groups in proper order
        for reasonstate in groups:
            line = groups[reasonstate]
            line['count'], line['nodename'] = compact_nodes(line['nodename'])
            line['date'] = time.strftime(timeformat,
                                         time.localtime(line['date']))
            printline.append(line)

    if ARGS_DEBUG:
        print('func:parse_properties returning values', file=sys.stderr)

    return printline


def format_strings(unformatted):
    '''Format strings for printing'''
    formatted = list()
    for length, string in unformatted:
        formatted.append('%-*s' % (length, string))
    return formatted


def print_overview(hosts=None):
    '''Print the default overview'''
    if ARGS_DEBUG:
        print('func:print_overview input:%s'
              % str(hosts), file=sys.stderr)

    # Set label and width for node properties
    labels = OrderedDict([('note', 'REASON'),
                          ('ticket', 'TICKET'),
                          ('username', 'USER'),
                          ('date', 'MODIFIED'),
                          ('count', 'COUNT'),
                          ('state', 'STATE'),
                          ('nodename', 'NODELIST')])
    widths = dict()
    for prop in labels:
        widths[prop] = 0

    # Create a sorted list of nodes with non-matching nodes first
    matched, rest = get_nodes(hosts)
    print_list = parse_properties(rest)
    print_list.extend(parse_properties(matched))

    # Stop here if there is nothing to show
    if not print_list:
        if not ARGS_QUIET:
            print('Nothing to show!')
        return

    # Determine the column widths
    for line in print_list:
        if line['ticket']:
            line['ticket'] = '#' + line['ticket']
        line['count'] = str(line['count'])
        for prop, label in labels.items():
            # Node count is only printed if nodes are clustered
            if prop == 'count' and line[prop] == '1':
                continue
            # Set width to full or truncated width
            proplen = len(line[prop])
            if line[prop] and proplen > widths[prop]:
                if not ARGS_LONG and label in TRUNC:
                    widths[prop] = min(proplen, TRUNC[label])
                else:
                    widths[prop] = proplen

    # Width can not be less than label length
    for prop, label in labels.items():
        if widths[prop] > 0:
            widths[prop] = max(widths[prop], len(label))

    # Print only columns that have values
    columns = []
    for prop, label in labels.items():
        if widths[prop] > 0:
            columns.append((widths[prop], label))
    print(' %s' % ' | '.join(format_strings(columns)))
    print('+'.join(['-' * (column[0] + 2) for column in columns]))

    # Prepare and print the node states and properties
    for line in print_list:
        line_columns = []
        for prop, label in labels.items():
            if widths[prop] > 0:
                if prop == 'count':
                    line[prop] = line[prop].rjust(widths[prop])
                elif line[prop] is None:
                    line[prop] = ''
                elif not ARGS_LONG and label in TRUNC:
                    if len(line[prop]) > TRUNC[label]:
                        line[prop] = line[prop][:TRUNC[label]-2]
                        line[prop] = line[prop] + '..'
                line_columns.append((widths[prop], line[prop]))
        print(' %s' % ' | '.join(format_strings(line_columns)))


def print_format(hosts=None, printformat=None):
    '''Print in custom format for scripting'''
    matched, rest = get_nodes(hosts)
    print_list = parse_properties(rest)
    print_list.extend(parse_properties(matched))

    for line in print_list:
        print(printformat % line)


class SaraNodes(object):
    '''This class is used to change node states or properties'''

    def __init__(self):
        self.ticket = None

    def _get_current_notes(self, nodes):
        '''Retrieve all nodes with notes and/or tickets'''
        if ARGS_DEBUG:
            print('class:SaraNodes func:_get_current_notes input:%s'
                  % str(nodes), file=sys.stderr)

        s = pyslurm.node()
        notes = dict()
        for node, properties in s.get().items():
            if node in nodes and properties['reason']:
                notes[node] = properties['reason'].split('+')
        return notes

    def _get_ticket(self, prev_ticket=None):
        '''Check if we already have a ticket number'''
        if ARGS_DEBUG:
            print('class:SaraNodes func:_get_ticket input:%s'
                  % prev_ticket, file=sys.stderr)
        cur_ticket = '%s' % self.ticket
        if prev_ticket and cur_ticket == prev_ticket:
            return prev_ticket
        elif self.ticket is not None:
            return cur_ticket
        elif prev_ticket:
            return prev_ticket
        return ''

    def _generate_note(self, nodes=None, note=None, append=True):
        '''Generate the note in a specific format'''
        if ARGS_DEBUG:
            print('class:SaraNodes func:_generate_note input:%s,%s,%s'
                  % (str(nodes), note, str(append)), file=sys.stderr)

        cur_data = self._get_current_notes(nodes)

        reasons = dict()
        for node in nodes:
            ticket = nnote = None
            # Get and update current ticket
            if node in cur_data.keys() and len(cur_data[node]) > 1:
                ticket = self._get_ticket(cur_data[node][0])
                nnote = cur_data[node][1:]
            else:
                ticket = self._get_ticket()
                nnote = []

            # Add, replace or append note
            if note is not None:
                if '+' in note:
                    print("Illegal character: '+'", file=sys.stderr)
                    exit(1)

                if not append or nnote == ['']:
                    nnote = [note]
                elif note and not note.lower() in map(unicode.lower, nnote):
                    nnote.append(note)

            reasons[node] = '%s+%s' % (ticket, '+'.join(nnote))
        return reasons

    def drain_node(self, nodes, note, append=True):
        '''Change the state of node(s) to offline with a specific note'''
        if ARGS_DEBUG:
            print('class:SaraNodes func:drain_node input:%s,%s'
                  % (str(nodes), note), file=sys.stderr)

        # again a loop, now create the attrib dict list
        batch_list = list()
        for node, note in self._generate_note(nodes, note, append).items():
            batch_list.append({'node_names': node,
                               'node_state': pyslurm.NODE_STATE_DRAIN,
                               'reason': note})
        self._process(batch_list)

    def resume_node(self, nodes):
        '''Clear the state on a node(s) to resume, also clear the note'''
        if ARGS_DEBUG:
            print('class:SaraNodes func:resume_node input:%s'
                  % str(nodes), file=sys.stderr)

        batch_list = list()
        drained = self._get_current_notes(nodes)

        # Resume node if it has a reason (otherwise do nothing)
        for node in nodes:
            if node in drained:
                batch_list.append({'node_names': node,
                                   'node_state': pyslurm.NODE_RESUME})
            elif ARGS_DEBUG:
                print('Node %s is already in an allowed state' % node)

        self._process(batch_list)

    def _process(self, batch_list):
        '''This function executes the change to the batch server'''
        if ARGS_DEBUG:
            print('class:SaraNodes func:_process input:%s'
                  % str(batch_list), file=sys.stderr)

        # If dry-run is not specified create a connection
        if not ARGS_DRYRUN:
            slurm = pyslurm.node()

        # Execute the changes
        for node in batch_list:
            if not ARGS_DRYRUN:
                try:
                    slurm.update(node)
                except:
                    print('Slurm error while updating node \'%s\''
                          % node, file=sys.stderr)
            else:
                print("slurm.update(%s)" % str(node))


if __name__ == '__main__':

    # Define and parse the input arguments
    HELP_EPILOG = '''
                  The format argument uses the Python string formatting. Fields
                  that can be used are; nodename, state, date, username, ticket
                  and note. For example slurm_nodes -f '%(nodename)s;%(state)s'
                  '''
    parser = argparse.ArgumentParser(description=__doc__, epilog=HELP_EPILOG)
    parser.add_argument('nodes', metavar='NODES', nargs='*', type=str)
    parser.add_argument('-j', '--jobs', action='store_true',
                        help='use job id\'s instead of nodenames')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='enables debug mode')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='enables verbose mode')
    parser.add_argument('-e', '--expand', action='store_true',
                        help='enables expanded hostnames')
    parser.add_argument('-i', '--ignore', action='store_true',
                        help='ignores nodes without partitions')
    parser.add_argument('-l', '--long', action='store_true',
                        help='prints long output of reasons and nodelists')
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help='enables dry-run mode')
    parser.add_argument('-q', '--quiet', action='store_true',
                        help='enables to supress all feedback')
    parser.add_argument('-o', '--offline', metavar='NOTE',
                        help='change state to offline with message')
    parser.add_argument('-m', '--modify', metavar='NOTE',
                        help='change the message of a node')
    parser.add_argument('-c', '--clear', action='store_true',
                        help='change the state to down')
    parser.add_argument('-N', '--clear-note', action='store_true',
                        help='clear the message of a node')
    parser.add_argument('-T', '--clear-ticket', action='store_true',
                        help='clear the ticket of a node')
    parser.add_argument('-f', '--format', metavar='FORMAT',
                        help='change the output of slurm_nodes (see footer)')
    parser.add_argument('-t', '--ticket', metavar='TICKET',
                        type=int, help='add a ticket number to a node')
    parser.add_argument('--version', action='version',
                        version=pyslurm.version())
    args = parser.parse_args()

    # The options quiet, ignore, debug, verbose and dry-run are processed first
    if args.quiet:
        ARGS_QUIET = True
    if args.debug:
        ARGS_DEBUG = True
    if args.verbose:
        ARGS_VERBOSE = True
    if args.ignore:
        ARGS_IGNORE = True
    if args.dry_run:
        ARGS_DRYRUN = ARGS_DEBUG = True
    if args.expand:
        ARGS_EXPAND = True
    if args.long:
        ARGS_LONG = True
    if not args.nodes:
        args.nodes = None
    if args.clear_ticket:
        args.ticket = ''

    if ARGS_DEBUG:
        print('func:__main__ checking type of operation', file=sys.stderr)

    # Determine if nodes or jobs were specified
    if args.nodes and args.jobs:
        args.nodes = get_exec_nodes(args.nodes)
    elif args.nodes:
        args.nodes = expand_nodes(args.nodes)

    # Initiate SaraNodes if we need to change node states or properties
    if args.offline or args.modify or args.clear or args.clear_note or args.clear_ticket or args.ticket:
        if not args.nodes:
            print('You did not specify any nodes, see --help', file=sys.stderr)
            sys.exit(1)

        sn = SaraNodes()
        if args.ticket is not None:
            sn.ticket = args.ticket

        if args.offline is not None:
            if ARGS_DEBUG:
                print('func:__main__ call sn.drain_node', file=sys.stderr)
            sn.drain_node(args.nodes, args.offline)
        elif args.modify is not None:
            if ARGS_DEBUG:
                print('func:__main__ call sn.drain_node', file=sys.stderr)
            sn.drain_node(args.nodes, args.modify, append=False)
        elif args.clear:
            if ARGS_DEBUG:
                print('func:__main__ call sn.resume_node', file=sys.stderr)
            sn.resume_node(args.nodes)
        elif args.clear_note:
            if ARGS_DEBUG:
                print('func:__main__ call sn.drain_node', file=sys.stderr)
            sn.drain_node(args.nodes, note='', append=False)
        elif args.ticket is not None:
            if ARGS_DEBUG:
                print('func:__main__ call sn.do_modify')
            sn.drain_node(args.nodes, '')
    else:
        if ARGS_DRYRUN:
            print('Dry-run is not available when querying', file=sys.stderr)

        if args.format:
            if ARGS_DEBUG:
                print('func:__main__ call print_format',
                      file=sys.stderr)
            print_format(args.nodes, args.format)
        else:
            if ARGS_DEBUG:
                print('func:__main__ call print_overview',
                      file=sys.stderr)
            print_overview(args.nodes)

    if ARGS_DEBUG:
        print('func:__main__ exit', file=sys.stderr)
