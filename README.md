# slurm\_nodes

You can use `slurm_nodes` to view and manage the state of your compute nodes,
with an optional reference to a ticket number from your internal ticket system.
It's also possible to query the state of one or multiple nodes in a custom
output format.

## Examples

### Show node states

```
martijk ~ $ slurm_nodes
 REASON                  | USER    | MODIFIED     | COUNT | STATE           | NODELIST
-------------------------+---------+--------------+-------+-----------------+-----------
 Down for some reason    | martijk | 16 Jan 13:38 |     2 | allocated+drain | r25n[4-5]
 Down for some very lo.. | martijk | 16 Jan 13:38 |     1 | allocated+drain | r25n6
```

Note that you can disable truncation by running `slurm_nodes -l`. Grouping of
nodes with a similar state can be disabled by running `slurm_nodes -e`.

```
martijk ~ $ slurm_nodes -e -l
 REASON                                                       | USER    | MODIFIED     | STATE           | NODELIST
--------------------------------------------------------------+---------+--------------+-----------------+----------
 Down for some reason                                         | martijk | 16 Jan 13:42 | allocated+drain | r25n4
 Down for some reason                                         | martijk | 16 Jan 13:42 | allocated+drain | r25n5
 Down for some very long reason which is truncated by default | martijk | 16 Jan 13:38 | allocated+drain | r25n6
```

To show the state of a specific node:

```
martijk ~ $ slurm_nodes r8n1
 MODIFIED     | STATE     | NODELIST
--------------+-----------+----------
 16 Jan 13:45 | allocated | r8n1
```

Or with a custom output:

```
martijk ~ $ slurm_nodes r8n1 -f '%(nodename)s|%(state)s'
r8n1|allocated
```

### Draining a node

```
martijk ~ $ slurm_nodes -o 'RAM issue' -t 12345 r26n1
martijk ~ $ slurm_nodes
 REASON                  | TICKET | USER    | MODIFIED     | COUNT | STATE           | NODELIST
-------------------------+--------+---------+--------------+-------+-----------------+-----------
 Down for some reason    |        | martijk | 16 Jan 13:40 |     2 | allocated+drain | r25n[4-5]
 Down for some very lo.. |        | martijk | 16 Jan 13:38 |     1 | allocated+drain | r25n6
 RAM issue               | #12345 | martijk | 16 Jan 13:40 |     1 | allocated+drain | r26n1
```

### Resuming a node

```
martijk ~ $ slurm_nodes -c r25n[4-5]
martijk ~ $ slurm_nodes
 REASON                  | TICKET | USER    | MODIFIED     | STATE           | NODELIST
-------------------------+--------+---------+--------------+-----------------+----------
 Down for some very lo.. |        | martijk | 16 Jan 13:38 | allocated+drain | r25n6
 RAM issue               | #12345 | martijk | 16 Jan 13:40 | allocated+drain | r26n1
```

### Other options

See `slurm_nodes --help` for all available options.

## Dependencies

* https://github.com/PySlurm/pyslurm
